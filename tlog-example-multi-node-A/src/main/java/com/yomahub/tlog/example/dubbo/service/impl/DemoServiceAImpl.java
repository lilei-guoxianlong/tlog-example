package com.yomahub.tlog.example.dubbo.service.impl;

import com.yomahub.tlog.example.dubbo.service.DemoService;
import com.yomahub.tlog.example.dubbo.service.DemoServiceA;
import com.yomahub.tlog.example.dubbo.service.DemoServiceB;
import com.yomahub.tlog.example.dubbo.service.DemoServiceC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("demoServiceA")
public class DemoServiceAImpl implements DemoServiceA {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private DemoServiceB demoServiceB;

    @Resource
    private DemoServiceC demoServiceC;

    @Override
    public String sayHello(String name) {
        log.info("node A");
        String str1 = demoServiceB.sayHello(name);
        String str2 = demoServiceB.sayMorning(name);
        String str3 = demoServiceC.sayEvening(name);
        return str1 + " | " + str2 + " | " + str3;
    }
}
