package com.yomahub.tlog.example.feign;

import com.yomahub.tlog.core.enhance.bytes.AspectLogEnhance;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class TLogFeignProviderRunner {

//    static {AspectLogEnhance.enhance();}

    public static void main(String[] args) {
        SpringApplication.run(TLogFeignProviderRunner.class, args);
    }
}
