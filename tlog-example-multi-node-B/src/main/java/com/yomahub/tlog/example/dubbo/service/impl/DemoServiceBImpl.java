package com.yomahub.tlog.example.dubbo.service.impl;

import com.yomahub.tlog.example.dubbo.service.DemoService;
import com.yomahub.tlog.example.dubbo.service.DemoServiceB;
import com.yomahub.tlog.example.dubbo.service.DemoServiceC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("demoServiceB")
public class DemoServiceBImpl implements DemoServiceB {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private DemoServiceC demoServiceC;

    @Override
    public String sayHello(String name) {
        log.info("node B");
        return demoServiceC.sayHello(name);
    }

    @Override
    public String sayMorning(String name) {
        return demoServiceC.sayMorning(name);
    }
}
