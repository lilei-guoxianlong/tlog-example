package com.yomahub.tlog.example.dubbo.service.impl;

import com.yomahub.tlog.core.annotation.TLogAspect;
import com.yomahub.tlog.example.vo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AspectDomain {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @TLogAspect({"id","label","user.name","user.userDetail.company","user.dddd"})
    public void test(Integer id, String label, String user){
        log.info("这是自定义表达标签");
    }

}
