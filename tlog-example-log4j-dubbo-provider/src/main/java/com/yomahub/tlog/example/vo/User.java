package com.yomahub.tlog.example.vo;

public class User {

    private Long id;

    private String name;

    private Integer age;

    private UserDetail userDetail;

    public User(Long id, String name, Integer age, UserDetail userDetail) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.userDetail = userDetail;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }
}
